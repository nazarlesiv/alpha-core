<?php
require_once('../../Alpha.php');

class AlphaTest extends PHPUnit_Framework_TestCase {

  protected $Alpha = null;

  public static function setUpBeforeClass() {

  }

  protected function setUp() {
    $this->Alpha = new Alpha();
  }

  public function test_App_should_exists() {
    $this->assertNotNull($this->Alpha, 'Should instantiate app.');
  }

  /**
   * @covers Alpha::module
   */
  public function test_should_add_module() {
    $this->assertEquals($this->Alpha->module(new stdClass()), $this->Alpha,
    'Should add a module and return a reference to self');

    $this->assertEquals(count($this->Alpha->injectables()), 1,
    'Should return the proper count of registry items');
  }

  /**
   * @covers Alpha::get
   */
  public function test_should_get_object_by_name() {
    $obj = new stdClass();
    $this->Alpha->module($obj);
    $this->assertEquals($obj, $this->Alpha->get(get_class($obj)),
    'Should return a proper object by name or alias');
  }

  /**
   * @covers Alpha::module
   */
  public function test_should_add_module_with_alias() {
    $obj = new stdClass();
    $alias = 'TestClass';
    $this->Alpha->module($alias, $obj);
    $this->assertEquals($obj, $this->Alpha->get($alias),
    'Should add and retrieve a module using an alias.');
  }
  /**
   * @covers Alpha::module
   */
  public function test_should_add_array_modules_with_alias() {
    $objOne = new stdClass();
    $objTwo = new stdClass();
    $objOneAlias = 'TestClassOne';
    $this->Alpha->module([
      [$objOneAlias, $objOne],
      $objTwo
    ]);
    $this->assertEquals($objOne, $this->Alpha->get($objOneAlias),
    'Should add and retrieve a module added in bulk using its alias.');
    $this->assertEquals($objTwo, $this->Alpha->get(get_class($objTwo)),
    'Should add and retrieve a module added in bulk using its class name.');
  }
  /**
   * @covers Alpha::module
   */
  public function test_should_throw_exception_for_wrong_param_order_multiple() {
    $this->expectException(Exception::class,
    'Alias and Module parameters must be in the correct order');

    $this->Alpha->module([
      [new stdClass(), 'alias'],
    ]);

    $this->expectException(Exception::class,
    'Alias and Module parameters must be in the correct order');

    $this->Alpha->module([
      new stdClass(), 'alias',
    ]);
  }

  /**
   * @covers Alpha::module
   * @group only
   */
  public function test_should_throw_exception_for_wrong_param_order_single() {
    $this->expectException(Exception::class,
    'Alias and Module parameters must be in the correct order');

    $this->Alpha->module(new stdClass(), 'alias');
  }

  /**
   * @covers Alpha::module
   */
  public function test_should_not_add_duplicate_module() {
    $obj = new stdClass();

    $this->Alpha->module($obj);
    $this->assertEquals($obj, $this->Alpha->get(get_class($obj)),
    'Should return a proper object by name or alias');

    $this->expectException(Exception::class,
    'Should throw exception when adding duplicated modules');
    $this->Alpha->module($obj);
  }

  /**
   * @covers Alpha::value
   */
  public function test_should_add_value() {
    $Alpha = $this->Alpha;
    $valOne = 'valOne';
    $valTwo = 'valTwo';
    $this->Alpha->value('value', $valOne);
    $this->assertEquals($Alpha->get('value'), $valOne,
    'Should be able to add a value');

    $this->Alpha->value('value', $valTwo);
    $this->assertEquals($Alpha->get('value'), $valTwo,
    'Should be able to replace value');

    $this->Alpha->value('value', null);
    $this->assertEquals($Alpha->get('value'), null,
    'Should be able to add a null value');

    $this->expectException(Exception::class,
    'Should throw exception when trying to add a none scalar or none array value');
    $this->Alpha->value('object', new stdClass());
  }

  /**
   * @covers Alpha::constant
   */
  public function test_should_add_constant() {
    $Alpha = $this->Alpha;
    $valOne = 'valOne';
    $valTwo = 'valTwo';
    $Alpha->constant('value', $valOne);
    $this->assertEquals($Alpha->get('value'), $valOne,
    'Should be able to add a value');

    $this->expectException(Exception::class,
    'Should throw exception when trying to replace a constant value');
    $Alpha->constant('value', $valTwo);
  }

  /**
   * @covers Alpha::inject
   */
  public function test_should_inject_values() {
    $Alpha = $this->Alpha;
    $objVal = new stdClass();
    $stringVal = 'valTwo';

    $injectedFunction = function($objVal, $stringVal) use ($Alpha) {
      $this->assertEquals($Alpha->get('objVal'), $objVal,
      'Should Inject a object value');
      $this->assertEquals($Alpha->get('stringVal'), $stringVal,
      'Should Inject a string value');
    };

    $Alpha->module('objVal', $objVal);
    $Alpha->value('stringVal', $stringVal);
    $Alpha->inject($injectedFunction);
  }

  /**
   * @covers Alpha::inject
   */
  public function test_should_defer_injected_function_execution() {
    $Alpha = $this->Alpha;
    $objVal = new stdClass();
    $stringVal = 'valTwo';

    $injectedFunction = function($objVal, $stringVal) use ($Alpha) {
      $this->assertEquals($Alpha->get('objVal'), $objVal,
      'Should Inject a object value');
      $this->assertEquals($Alpha->get('stringVal'), $stringVal,
      'Should Inject a string value');
    };

    $Alpha->module('objVal', $objVal);
    $Alpha->value('stringVal', $stringVal);

    $deferred = $Alpha->inject($injectedFunction, true);

    $this->assertTrue(is_callable($deferred),
    'Should return a callable wrapper.');

    $deferred();
  }

  /**
   * @covers Alpha::inject
   */
  public function test_should_throw_exception_for_missing_value() {
    $Alpha = $this->Alpha;
    $objVal = new stdClass();
    $stringVal = 'valTwo';

    $Alpha->module($objVal);
    $Alpha->value('stringVal', $stringVal);

    $injectedFunction = function($objVal, $stringVal) use ($Alpha) {};

    $this->expectException(Exception::class,
    'Should throw exception if value does not exist');
    $Alpha->inject($injectedFunction);
  }
}
