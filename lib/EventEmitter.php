<?php
class EventEmitter {
  //Listeners array.
  private $_listeners = [];

  public function __construct() {}

  public function emit($e, $v) {
    if (!is_string($e) || empty($e)) {
      return false;
    }
    if (isset($this->_listeners[ $e ]) && is_array($this->_listeners[ $e ])) {
      $count = count($this->_listeners[ $e ]);
      //todo: Build a proper, informative event object to be passed along with the call.
      for ($i = 0; $i < $count; ++$i) {
        $this->_listeners[ $e ][$i]($e, $v);
      }
    }
  }

  public function on($e, $fn) {
    //Check that the even name is a valid string.
    if (!is_string($e) || empty($e) || !is_callable($fn)) {
      return false;
    }
    if (isset($this->_listeners[ $e ])) {
      $this->_listeners[ $e ][] = $fn;
    } else {
      $this->_listeners[ $e ] = [$fn];
    }
  }
}
