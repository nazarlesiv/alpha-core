<?php
require_once('lib/EventEmitter.php');

class Alpha
  extends EventEmitter {

  //Dynamically added methods that modules decorate the app object with.
  private $_fn   = [];
  private $_prop = [];

  //Generic container.
  private $_registry = [];

  public function __construct() { }

  public function __destruct() { }

  /*
   *@access public
   *@method inject() - Invokes the supplied function with the expected parameters.
   *@param function $fn - Function into which parameters are to be injected.
   *@param Boolean $defer - If set to true, will return a function with the injected
      parameters for later executiong.
   *@return Mixed - If immediately invoked, returns the return value from the supplied
    function. Otherwise returns a function with a references to the required parameters.
   */
  public function inject($fn, $defer = false) {
    if(!is_callable($fn)) {
      throw new exception('Invalid Function Supplied - must be callable.');
    }
    // Get the function metadata.
    $meta = new ReflectionFunction($fn);
    $params = $meta->getParameters();
    $count = count($params);
    $args = [];
    if($count) {
      for($i = 0; $i < $count; ++$i) {
        // Throw an exception if a matching module is not found?
        // or assing a NULL to the value?
        $name = $params[$i]->name;

        // Check if exists.
        if(!isset($this->_registry[$name])) {
          throw new exception("Injection Failed, cannot find '{$name}'.");
        }

        $args[] = $this->_registry[$name]['value'];
      }
    }
    // If the function is being deferred, return a wrapper.
    if($defer === true) {
      return function() use ($fn, $args) {
        return call_user_func_array($fn, array_merge($args, func_get_args()));
      };
    }
    return call_user_func_array($fn, $args);
  }

  public function injectables() {
    return ($temp = $this->_registry);
  }

  public function get($key) {
    if(!is_string($key)) {
      return null;
    }
    return isset($this->_registry[$key]) ? $this->_registry[$key]['value'] : null;
  }

  public function value($key, $value, $const = false) {

    if(!is_scalar($key)) {
      throw new exception('Key parameter must be scalar.');
    }

    if(!is_null($value) && !is_scalar($value) && !is_array($value)) {
      throw new exception('Value parameter must be scalar or array.');
    }

    if(!is_bool($const)) {
      throw new exception('Const parameter must be boolean.');
    }

    $this->_store($key, $value, ['const' => $const]);

    return $this;
  }

  // Helper method for setting a value with the constant param set to true.
  public function constant($key, $value) {
    return $this->value($key, $value, true);
  }

  public function module() {
    $alias = null;
    $module = null;

    // Get the list of parameters passed to the function.
    $args = func_get_args();

    /*
     Validate the parameter types based on the
     number of Supplied values.
     */
    switch(count($args)) {
      case 1:
        if(is_object($args[0]) || is_array($args[0])) {
          $module = $args[0];
        } else {
          throw new exception('Invalid Module Supplied');
        }
        $module = $args[0];
      break;

      case 2:
        if(is_string($args[0]) && is_object($args[1])) {
          $alias = $args[0];
          $module = $args[1];
        } else {
          throw new exception('Invalid Module Supplied');
        }
      break;

      default:
        throw new exception('Invalid Module Supplied');
      break;
    }

    // Default properties for modules.
    // Constant.
    $props = ['const' => true];

    if(is_array($module)) {
      foreach($module as $mod) {
        // Check if $mod is array.
        // Use second value as the name.
        if(is_array($mod) && count($mod) === 2 &&
          is_scalar($mod[0]) && is_object($mod[1])) {
          $this->_store($mod[0], $mod[1], $props);
        } else if(is_object($mod)) {
          $this->_store(get_class($mod), $mod, $props);
        } else {
          throw new exception('Invalid Module Supplied');
        }
      }
    } else if(is_object($module)) {
      $key = !is_null($alias) && is_scalar($alias) ? $alias : get_class($module);
      $this->_store($key, $module, $props);
    }
    return $this;
  }

  private function _store ($key, $value, $props = []) {
    // Check if a value with the same name exists.
    if(isset($this->_registry[$key])) {
      $obj = $this->_registry[$key];
      // Check if the value is not a constant.
      if(isset($obj['const']) && $obj['const'] === true) {
        throw new exception("Cannot override constant '{$key}'");
      }
    }
    $this->_registry[$key] = array_merge(['value' => $value], $props);
    return true;
  }

  public function __set($name, $val) {
    if (empty($name) || !is_string($name) || empty($val)) {
      return false;
    }

    //Check if the value is callable or is an object.
    if (is_callable($val) || is_object($val)) {
      return $this->fn($name, $val);
    } else {
      //Value is scalar, store in the properties array.
      return $this->prop($name, $val);
    }
  }

  public function __get($name) {

  }

  public function __call($name, $arguments) {
    if (isset($this->_fn[ $name ])) {
      $fn = $this->_fn[ $name ];
    } else {
      throw new Exception('Call to an undefined method \'' . $name . '\' in ' . __FUNCTION__);
    }

    //Check if the fn is a callable function.
    if (is_callable($fn)) {
      return call_user_func_array($fn, $arguments);
    }

    //Check if fn is an object.
    if (is_object($fn)) {
      //call a function in the _fn array and pass the arguments.
      return call_user_func_array([$this->_fn[ $name ], $name], $arguments);
    }
  }

  public function __isset($name) {
    // TODO: Implement __isset() method.
    return isset($this->_fn[ $name ]);

  }
  /*
   * load the requested route.
   */
  public function run($fn) {
    $this->inject($fn);
    return $this;
  }

  private function splitPathUri($uri) {
    //split the value on either / or \.
    return preg_split('/[\.\/]/', $uri);
  }

  //Decorator method that adds a function to the app instance.
  public function fn($name, $fn = null) {
    if (!is_string($name)) {
      return null;
    }

    //If the function is not provided, try to retrieve and return a helper by the supplied name.
    if (is_null($fn)) {
      return isset($this->_fn[ $name ]) ? $this->_fn[ $name ] : null;
    }

    $this->_fn[ $name ] = $fn;

    return $this;
  }

  //Property Decorator
  public function prop($name, $val = null) {
    if (!is_string($name)) {
      return null;
    }
    if (is_null($val)) {
      return isset($this->_prop[ $name ]) ? $this->_prop[ $name ] : null;
    }

    $this->_prop[ $name ] = $val;

    return $this;
  }
}
